FROM denoland/deno:latest as base
EXPOSE 8080
WORKDIR /restapiwithdeno
COPY . ./
RUN deno cache server.ts
CMD ["run", "--allow-net", "server.ts"]
